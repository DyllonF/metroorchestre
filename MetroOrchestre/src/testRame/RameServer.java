package testRame;

import metroorchestre.RameImpl;

public class RameServer {
	public static void main(String[] args) throws Exception {
		
		java.rmi.registry.LocateRegistry.createRegistry(Integer.valueOf(9000));
		java.rmi.Remote rame = new RameImpl(1);
		java.rmi.Naming.bind("rmi://localhost:9000/rame",rame);
		java.rmi.Remote rame2 = new RameImpl(2);
		java.rmi.Naming.bind("rmi://localhost:9000/rame2",rame2);
		java.rmi.Remote rame3 = new RameImpl(3);
		java.rmi.Naming.bind("rmi://localhost:9000/rame3",rame3);
	}

}
