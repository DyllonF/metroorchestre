package testRame;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import metroorchestre.Rame;

public class RameTest  {
	public static void main(String[] args) throws InterruptedException {
		try {
			Rame r = (Rame) java.rmi.Naming.lookup("rmi://localhost:9000/rame");
			r.demarrer();
			r.fermerPorte();
			r.actionnerMoteur(3000);
			r.ouvrirPorte();
			Rame r2 = (Rame) java.rmi.Naming.lookup("rmi://localhost:9000/rame2");
			r2.demarrer();
			r2.fermerPorte();
			r2.actionnerMoteur(3000);
			r2.ouvrirPorte();
			Rame r3 = (Rame) java.rmi.Naming.lookup("rmi://localhost:9000/rame3");
			r3.demarrer();
			r3.fermerPorte();
			r3.actionnerMoteur(3000);
			r3.ouvrirPorte();
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
