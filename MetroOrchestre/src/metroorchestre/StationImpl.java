package metroorchestre;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import afficheurmetro.MetroSuperviseurIHM;

@SuppressWarnings("serial")
public class StationImpl extends UnicastRemoteObject implements Station {
	private Voie[] lesVoies;
	private String nom;
	private int numero;

	public StationImpl(String nom, int i) throws RemoteException {
		this.nom = nom;
		this.numero = i;
	}

	public int getNumero() {
		return this.numero;
	}

	@Override
	public String afficher() throws RemoteException {
		return null;
	}

	@Override
	public String getNom() throws RemoteException {
		return this.nom;
	}

	@Override
	public void ajouterStationSuivante(String machine, int port, int numeroVoieDepart, String stationSuivante,
			int voieSuivante) throws RemoteException, NotBoundException, MalformedURLException {

	}

	@Override
	public boolean estFeuVert(int numeroVoie) throws RemoteException {
		return this.lesVoies[numeroVoie].estVert();
	}

	@Override
	public void demarrerRame(int numeroVoie) throws RemoteException {
		int numRame = this.lesVoies[numeroVoie].getRame().getNumero();

		try {
			this.lesVoies[numeroVoie].getRame().demarrer();
			MetroSuperviseurIHM superviseur = (MetroSuperviseurIHM) Naming.lookup("rmi://localhost:9999/superviseur");
			superviseur.modifierAffichage(this.nom, numeroVoie, this.getNomStationSuivante(numeroVoie),
					this.getNumeroVoieSuivante(numeroVoie), Integer.toString(numRame));

		} catch (InterruptedException | MalformedURLException | NotBoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getNomStationSuivante(int numeroVoie) throws RemoteException {
		return this.lesVoies[numeroVoie].getNomStationSuivante();
	}

	@Override
	public int getNumeroVoieSuivante(int numeroVoie) throws RemoteException {
		return this.lesVoies[numeroVoie].getNumeroVoieSuivante();
	}

	@Override
	public void setRame(int numeroVoie, Rame rame) throws RemoteException {
		this.lesVoies[numeroVoie].setRame(rame);
	}

	@Override
	public void allumerFeuRouge(int numeroVoie) throws RemoteException {
		this.lesVoies[numeroVoie].allumerFeuRouge();

	}

	@Override
	public int getNumeroVoie(Rame rame) throws RemoteException {
		for (Voie lesVoie : this.lesVoies) {
			if (lesVoie.estRamePresente(rame)) {
				return lesVoie.getNumeroVoie();
			}
		}
		return -1;
	}

}
