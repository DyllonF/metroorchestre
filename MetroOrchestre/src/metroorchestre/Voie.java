package metroorchestre;

import java.rmi.RemoteException;

public class Voie implements Runnable {

	@SuppressWarnings("unused")
	private Thread GestionnaireRame;
	private Rame laRame;
	public Station stationSuivante;
	private int numeroVoieSuivante;
	private int numeroVoie;
	private etat etatcourant;

	private enum etat {
		vert, rouge
	};

	public Voie(int i) {
		this.numeroVoie = i;
		this.allumerFeuVert();
	}

	public int getNumeroVoie() {
		return this.numeroVoie;
	}

	public void setNumeroVoie(int numeroVoie) {
		this.numeroVoie = numeroVoie;
	}

	public boolean estVert() {
		return this.etatcourant == etat.vert;

	}

	public void setRame(Rame rame) {
		this.laRame = rame;

	}

	public Rame getRame() {
		return this.laRame;
	}

	public boolean estRamePresente(Rame rame) {
		return this.laRame == rame;
	}

	public void allumerFeuRouge() {
		this.etatcourant = etat.rouge;

	}

	public void allumerFeuVert() {
		this.etatcourant = etat.vert;

	}

	public void ajouterStationSuivante(String url, int port, String stationSuivante, int voieSuivante)
			throws Exception {
		this.stationSuivante = (Station) java.rmi.Naming.lookup("rmi://" + url + ":" + port + "/" + stationSuivante);
		this.numeroVoieSuivante = voieSuivante;
	}

	public String getNomStationSuivante() throws RemoteException {
		return this.stationSuivante.getNom();
	}

	public void demarrerRame() {

		// TODO : C VIDE

	}

	public int getNumeroVoieSuivante() throws RemoteException {
		return this.numeroVoieSuivante;
	}

	@Override
	public void run() {
		try {
			this.laRame.demarrer();
			this.laRame.fermerPorte();
			Voie.this.setRame(null);
			this.laRame.actionnerMoteur(300);
			this.allumerFeuVert();
			this.stationSuivante.setRame(this.numeroVoieSuivante, this.laRame);
			this.laRame.ouvrirPorte();
			Thread.sleep(3000);
			this.laRame.departImminent();
		} catch (RemoteException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
